#include "matrix.hpp"
#include "graph.h"

// Edge

Edge::Edge(edge_ind_t src, edge_ind_t dest, weight_t weight) : weight(weight), source(src), destination(dest) {}

Edge::Edge() : weight(0), source(0), destination(0) {}

bool operator> (const Edge &e1, const Edge &e2) {
    if (e1.weight == Edge::inf || e2.weight == Edge::inf) {
        return e2.weight == Edge::inf ? false : true;
    }
    return e1.weight > e2.weight;   
}

bool operator< (const Edge &e1, const Edge &e2) {
    if (e1.weight == Edge::inf || e2.weight == Edge::inf) {
        return e1.weight == Edge::inf ? false : true;
    }
    return e1.weight < e2.weight; 
}

bool operator== (const Edge &e1, const Edge &e2) {
    return e1.weight == e2.weight;
}

Edge operator+ (const Edge &e1, const Edge &e2) {
    weight_t result_weight = e1.weight == Edge::inf || e2.weight == Edge::inf ? Edge::inf : e1.weight + e2.weight;
    return Edge(e1.source, e2.destination, result_weight);
}

// Graph

graph_t Graph::get_verticies() {
    return verticies;
}

Matrix<weight_t> Graph::get_weight_matrix() {
    Matrix<weight_t> result(verticies, verticies, 0);
    for (graph_t i = 0; i < verticies; i++) {
        for (graph_t j = 0; j < verticies; j++) {
            auto range = graph_container.equal_range(std::pair<graph_t, graph_t>(i,j));
            if (range.first == range.second) {
                result[i][j] = -1;
            } else {
                Edge min_edge = range.first->second;
                auto iter = range.first;
                for (iter++; iter != range.second; iter++) {
                    if (iter->second.weight < min_edge.weight) {
                        min_edge = iter->second;
                    } 
                }
                result[i][j] = min_edge.weight;
            }
        }
    }
    return result;
}

Matrix<Edge> Graph::get_shortest_edge_matrix() {
    Matrix<Edge> result(verticies, verticies);
    for (graph_t i = 0; i < verticies; i++) {
        for (graph_t j = 0; j < verticies; j++) {
            auto range = graph_container.equal_range(std::pair<graph_t, graph_t>(i,j));
            if (range.first == range.second) {
                result[i][j] = Edge(i,j,Edge::inf);
            } else {
                Edge min_edge = range.first->second;
                auto iter = range.first;
                for (iter++; iter != range.second; iter++) {
                    if (iter->second.weight < min_edge.weight) {
                        min_edge = iter->second;
                    } 
                }
                result[i][j] = min_edge;
            }
        }
    }
    return result;
}

void Graph::add_edge(const Edge &e) {
    graph_container.insert(std::make_pair(std::make_pair(e.source, e.destination), e));
    verticies = verticies < std::max(e.source + 1, e.destination + 1) ? std::max(e.source + 1, e.destination + 1) : verticies;  
}

Graph::edge_multimap::const_iterator Graph::cbegin() const {
    return graph_container.cbegin(); 
}

Graph::edge_multimap::const_iterator Graph::cend() const {
    return graph_container.cend(); 
}

Graph::edge_multimap::size_type Graph::size() const {
    return graph_container.size();
}

Graph::edge_multimap::iterator Graph::begin() {
    return graph_container.begin();
}

Graph::edge_multimap::iterator Graph::end() {
    return graph_container.end();
}
