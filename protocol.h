#ifndef PROTOCOL_HEADER
#define PROTOCOL_HEADER

#include <sys/types.h>
#include <sys/socket.h>
#include <algorithm>

#include "graph.h"
#include "matrix.hpp"

namespace Protocol {

class Message;
class Data;
class DGraph;
class DText;
class DUnum;

using field_t = unsigned long;
using mess_t = unsigned long;

enum class message_type : mess_t {
    ERROR = 0,
    SERVICE,
    DATA_GRAPH,
    DATA_TEXT,
    DATA_UNUM,
    CR,
    SR,
    UNKNOWN
};

enum class dedge_type : unsigned char {
    ERR,
    STD,
    END
};

namespace Buffer {
    constexpr int msg_len = 31;
    constexpr int text_len = 100;
    constexpr int edge_len = 34;
}

namespace SocketAcceptors {
    int accept_message(int socketfd, Message &mes);
    int accept_data_graph(int socketfd, DGraph &dgr);
    int accept_data_text(int socketfd, DText &dtxt);
    int accept_data_unum(int socketfd, DUnum &num);
}

namespace SocketSenders {
    int send_message(int socketfd, Message &mes);
    int send_data_graph(int socketfd, DGraph &dgr);
    int send_data_text(int socketfd, DText &dtxt);
    int send_data_unum(int socketfd, DUnum &num);
}

class Message {
    message_type type;
    field_t param;
    friend int SocketAcceptors::accept_message(int socketfd, Message &mes);
public:
    Message();
    Message(message_type t, field_t p);
    message_type get_type();
    field_t get_parameter();
    static Message parse_message(const std::string &mes_str);
};

class TEdge : public Edge {
    dedge_type type;
public:
    TEdge(edge_ind_t src, edge_ind_t dest, weight_t weight, dedge_type t);
    TEdge(Edge e);
    inline dedge_type get_type() const;
    static TEdge parse_edge(std::string str);
    std::string to_string() const;
    static bool is_end_marker(std::string &str);
};

class Data {
public:
    virtual ~Data() {};
};

class DGraph : public Data  {
public:
    Graph graph; 
    ~DGraph() {};
};

class DText : public Data {
public:
    std::string text;
    ~DText() {};
};  

class DUnum : public Data {
public:
    field_t number;
    ~DUnum() {};
};

message_type hash_message_type(std::string str);
std::string unhash_message_type(message_type t);
bool is_unumber(const std::string &str);
int recv_all(int socketfd, int message_len, char *buf, size_t buf_size);
int send_all(int socketfd, int message_len, const char *buf, size_t buf_size);

}

#endif
