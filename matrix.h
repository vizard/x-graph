#ifndef MATRIX_H_HEADER
#define MATRIX_H_HEADER

#include <vector>

template <class T>
class Matrix {
    unsigned long rows, columns;
    std::vector<std::vector<T>> array;
public:
    Matrix(unsigned long row, unsigned long col, const T val = T());
    std::vector<T> &operator[](unsigned long index);
    const std::vector<T> &operator[](unsigned long index) const;
    unsigned long row_size() const;
    unsigned long col_size() const;
};

#endif
