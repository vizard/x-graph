#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <cstdlib>
#include <iostream>

#include <vector>
#include <map>
#include <string>

#include "protocol.h"

#include <ncurses.h>

namespace Interface {
   
    class Window;
    class Menu;
    class Info;
    class EnterField; 

    int start_interface();
    int stop_interface();
    void clean_screen();
   
    
    class Window {
        virtual void show() = 0;
    public:
        virtual int activate() = 0;
        virtual ~Window() {};
    };

    class Value;

    class Menu : public Window {
    protected:
        using menu_items = std::vector<std::pair<std::string, std::string>>;
        menu_items items;
        menu_items::size_type pointer = 0;
    public:
        Menu() {};
        Menu(std::vector<std::string> itm);
        Menu(std::vector<Value*> val_itm);
        void show() override;
        int activate() override;
        ~Menu() override {};
    };

    class Value {
    public:
        const std::string name;
        virtual std::string get_value_str() {
            return std::string("");
        };
        Value(std::string n) : name(n) {}
        virtual ~Value() {};
    };

    class FixedValue : public Value {
        unsigned actual_value = 0;
        const std::vector<std::string> possible_values;
    public:
        FixedValue(std::string n, std::vector<std::string> pos_vals);
        void next_value();
        std::string get_value_str() override;
        ~FixedValue() override {}
    };

    class VarValue : public Value {
    public:
        std::string str_val;
        VarValue(std::string n, std::string val = std::string());
        std::string get_value_str() override;
        ~VarValue() override {} 
    };

    class ValuesPanel : public Menu {
        std::vector<Value*> values_set;
        Value apply_button = Value("apply");
        void show() override;
    public:
        ValuesPanel(std::vector<Value*> v_s); 
        int activate() override;
        ~ValuesPanel() override {};
    };

    FixedValue::FixedValue(std::string n, std::vector<std::string> pos_vals) : Value(n), possible_values(pos_vals) {}
    
    void FixedValue::next_value() {
        actual_value = (actual_value + 1) % possible_values.size();
    }

    std::string FixedValue::get_value_str() {
        return possible_values[actual_value]; 
    }

    VarValue::VarValue(std::string n, std::string val) : Value(n), str_val(val) {}

    std::string VarValue::get_value_str() {
        return str_val;
    }

    ValuesPanel::ValuesPanel(std::vector<Value*> v_s) : Menu(v_s), values_set(v_s) {}

    void ValuesPanel::show() {
        clean_screen();
        for (menu_items::size_type item_ind = 0; item_ind < items.size(); item_ind++) {
            std::string out_str;
            if (item_ind == pointer) {
                out_str += items[item_ind].second;
            } else {
                out_str += items[item_ind].first;
            }
            out_str += values_set[item_ind]->get_value_str() + "\n";
            addstr(out_str.c_str());
        }
        if (pointer == items.size()) {
            addstr("> Apply\n");
        } else {
            addstr("Apply\n");
        }
        refresh();
    }

       
    class Info : public Window {
        std::string text;
        void show() override;
    public:
        Info(std::string txt); 
        int activate() override;
        ~Info() override {};
    };

    class EnterField : public Window {
        static constexpr unsigned max_inp_size = 100;
        const std::string prompt;
        char result[max_inp_size+1] = {'\0'};
        void show() override;
    public:
        EnterField(std::string pr);
        ~EnterField() override {};
        int activate() override;
        std::string get_result();
    };
    
    int start_interface() {
        initscr();
        cbreak();
        noecho();
        keypad(stdscr, TRUE);
        return 0;
    }

    int stop_interface() {
        endwin();
        return 0;
    }

    void clean_screen() {
        clear();
    }

    EnterField::EnterField(std::string pr) : prompt(pr) {};
    
    std::string EnterField::get_result() {
        result[max_inp_size] = '\0';
        return std::string(result);
    }
    
    void EnterField::show() {
        clean_screen();
        addstr((prompt + " ").c_str());
        refresh();
    }

    int EnterField::activate() {
        show();
        echo();
        getnstr(result, max_inp_size);
        noecho();
        return 0;
    }

    Menu::Menu(std::vector<std::string> itm) {
        for (auto ptr : itm) {
            items.push_back(std::pair<std::string, std::string>(ptr, "> " + ptr));
        }    
    }
        
    Menu::Menu(std::vector<Value*> val_itm) {
        for (auto val_ptr : val_itm) {
            items.push_back(std::pair<std::string, std::string>(val_ptr->name + ": ", "> " + val_ptr->name + ": "));
        }
    }

    void Menu::show() {
        clean_screen();
        for (menu_items::size_type item_ind = 0; item_ind < items.size(); item_ind++) {
            if (item_ind == pointer) {
                addstr(items[item_ind].second.c_str());
            } else {
                addstr(items[item_ind].first.c_str());
            }
        }
        refresh();
    }

    int Menu::activate() {
        curs_set(0);
        show();
        while(true) {
            int com = getch();
            switch (com) {
                case KEY_UP:
                    pointer = !pointer ? items.size()-1:pointer-1;
                    break;
                case KEY_DOWN:
                    pointer = (pointer + 1) % items.size();
                    break;
                case '\n':
                    return pointer;
                    break;
                default:
                    break;
            }
            show();
        }
        curs_set(1);
        return 0;
    };     
    
    int ValuesPanel::activate() {
        curs_set(0);
        show(); 
        while (true) {
            int com = getch();
            switch (com) {
                case KEY_UP:
                    pointer = !pointer ? items.size():pointer-1;
                    break;
                case KEY_DOWN:
                    pointer = (pointer + 1) % (items.size()+1);
                    break;
                case '\n':
                    if (pointer == items.size()) { // Apply chosen
                        return 0;
                    } else if (dynamic_cast<FixedValue*>(values_set[pointer])) {
                        FixedValue *cur_val_ptr = dynamic_cast<FixedValue*>(values_set[pointer]);
                        cur_val_ptr->next_value();
                    } else if (dynamic_cast<VarValue*>(values_set[pointer])) {
                        VarValue *cur_val_ptr = dynamic_cast<VarValue*>(values_set[pointer]);
                        EnterField enter_window = EnterField(std::string("Set ") + cur_val_ptr->name + ": ");
                        enter_window.activate();
                        cur_val_ptr->str_val = enter_window.get_result();
                    } 
                    break;
                default:
                    break;
            }
            show();
        }
        curs_set(1);
        return 0;  
    }

    Info::Info(std::string txt) : text(txt) {}

    void Info::show() {
        clean_screen();
        addstr(text.c_str());
        refresh();
    }

    int Info::activate() {
        show();
        while(true) {
            int com = getch();
            switch(com) {
                case '\n':
                    return 0;
                    break;
                default:
                    break;
            }
        }
    }

    class GraphEnter : public Window {
        Protocol::DGraph input_graph;
        void show() override;
        public:
        int activate() override;
        std::vector<std::pair<std::string,Graph::edge_multimap::iterator>> get_edges_list();
        Protocol::DGraph get_dgraph_result();
        ~GraphEnter() override {};
    };

    Protocol::DGraph GraphEnter::get_dgraph_result() {
        return input_graph;
    }

    void GraphEnter::show() {
        clean_screen();
        addstr("Press N to add new edge or R when ready...\n");
        auto edges_list = get_edges_list();
        for (auto ptr : edges_list) {
            addstr((ptr.first + "\n").c_str());
        }
        refresh(); 
    }

    int GraphEnter::activate() {
        input_graph = Protocol::DGraph();
        curs_set(0);
        show();
        while(true) {
            int com = getch();
            switch (com) {
                case 'R':
                    return 0;
                    break;
                case 'N':
                    EnterField edge_enter("Enter new edge: ");
                    edge_enter.activate();
                    std::string edge_str = edge_enter.get_result();
                    std::string src, dst, weight;
                    if (std::count(edge_str.begin(), edge_str.end(), ',') != 2) {
                        Info i("Invalid format.");
                        i.activate();
                        break;
                    }
                    src = edge_str.substr(0,edge_str.find(',',0));
                    edge_str.erase(0,edge_str.find(',',0)+1);
                    dst = edge_str.substr(0,edge_str.find(',',0));
                    edge_str.erase(0,edge_str.find(',',0)+1);
                    weight = edge_str;
                    try {
                    input_graph.graph.add_edge(Edge(static_cast<edge_ind_t>(std::stoul(src)), static_cast<edge_ind_t>(std::stoul(dst)), static_cast<weight_t>(std::stoul(weight))));
                    } catch (...) {
                        Info i("Invalid format.");
                        i.activate();
                        break;
                    }
                    break;
            }
            show();
        }
        curs_set(1);
    }

std::vector<std::pair<std::string,Graph::edge_multimap::iterator>> GraphEnter::get_edges_list() {
    std::vector<std::pair<std::string,Graph::edge_multimap::iterator>> edges_list;
    for (auto itr = input_graph.graph.begin(); itr != input_graph.graph.end(); itr++) {
        edges_list.push_back(std::pair<std::string, Graph::edge_multimap::iterator>(std::string("(") + std::to_string((itr->second).source) + "," + std::to_string((itr->second).destination) + "," + std::to_string((itr->second).weight) + ")", itr));
    }
    return edges_list;
}

}



void client_exit(int code) {
    Interface::stop_interface();
    exit(code);
}

using namespace Interface;

int main() {

    try {

    std::map<std::string, std::vector<std::string>&> menu_item_sets;

    std::vector<std::string> main_menu_items = 
    {
        "Shortest path search\n",
        "Settings\n",
        "Exit\n"
    };

    std::vector<std::string> settings_menu_items = 
    {
        "Shortest path algorithm\n",
        "Addirional info show\n",
        "Server IP\n",
        "Server Port\n"
    };

    FixedValue algorithm("algo", std::vector<std::string> {"Floyd-Warshall", "Dijkstra(not implemented)"});
    FixedValue addinfo_show("addinfo", std::vector<std::string> {"Show", "Hide"});
    VarValue server_addr("ip", "127.0.0.1");
    VarValue server_port("port", "25000");
    std::vector<Value*> all_settings;
    all_settings.push_back(&algorithm);
    all_settings.push_back(&addinfo_show);
    all_settings.push_back(&server_port);
    all_settings.push_back(&server_addr);

    start_interface();

    while (true) {
        Menu main_menu(main_menu_items);
        int p = main_menu.activate();
        switch (p) {
            case 0:
                {
                    Protocol::DUnum src, dst;
                    bool ok = false;
                    while (!ok) {
                        EnterField ent_src("Source vertex number (from zero): ");
                        ent_src.activate();
                        try {
                            long long candidate = stoll(ent_src.get_result());
                            if (candidate < 0) throw 1;
                            src.number = candidate;
                        } catch (...) {
                            Info i("Invalid value.");
                            i.activate();
                            continue;
                        }
                        ok = true;
                    }
                    ok = false;
                    while (!ok) {
                        EnterField ent_dst("Destination vertex number (from zero): ");
                        ent_dst.activate();
                        try {
                            long long candidate = stoll(ent_dst.get_result());
                            if (candidate < 0) throw 1;
                            dst.number = candidate;
                        } catch(...) {
                            Info i("Invalid value.");
                            i.activate();
                            continue;
                        }
                        ok = true;
                    }

                    GraphEnter g;
                    g.activate();
                    Protocol::DGraph to_send_dgraph = g.get_dgraph_result();

                    struct addrinfo *client;
                    struct addrinfo client_hints;
                    
                    memset(static_cast<void *>(&client_hints), 0, sizeof(client_hints));
                    client_hints.ai_family = AF_INET;
                    client_hints.ai_socktype = SOCK_STREAM;
                    client_hints.ai_protocol = getprotobyname("tcp")->p_proto;
                    if(getaddrinfo(server_addr.get_value_str().c_str(), server_port.get_value_str().c_str(), &client_hints, &client)){
                        Info i("Unsiccessfull collection of information about the server.");
                        i.activate();
                        break;
                    }
                    
                    int sockfd = socket(client->ai_family, client->ai_socktype, client->ai_protocol);
                    if (sockfd == -1) {
                        Info i("Error occured during socket creation.");
                        i.activate();
                        break;
                    }
                    
                    if(connect(sockfd, client->ai_addr, client->ai_addrlen) == -1) {
                        Info i("Error during connection.");
                        i.activate();
                        break;
                    }
                    
                    Protocol::Message serv_mes(Protocol::message_type::SERVICE,1); //??
                    Protocol::SocketSenders::send_message(sockfd, serv_mes);
                    
                    
                    Protocol::SocketSenders::send_data_unum(sockfd, src);
                    Protocol::SocketSenders::send_data_unum(sockfd, dst);
                    Protocol::SocketSenders::send_data_graph(sockfd, to_send_dgraph);
                    
                    Protocol::DUnum result;
                    Protocol::DText path, details;
                    
                    Protocol::SocketAcceptors::accept_data_unum(sockfd, result);
                    Protocol::SocketAcceptors::accept_data_text(sockfd, path);
                    Protocol::SocketAcceptors::accept_data_text(sockfd, details);
                   
                    std::string result_text = std::string("Shortest path value: ") + std::to_string(result.number) + "\n" + std::string("Shortest path description: ") + path.text + "\n";
                    if (addinfo_show.get_value_str() == "Show") {
                        result_text += std::string("Other details (-1 is infinity): \n") + details.text + "\n"; 
                    }
                    Info result_info(result_text);
                    result_info.activate();
                    freeaddrinfo(client);
                    close(sockfd);
                }
                break;
            case 1:
                {
                    bool is_ok = false;
                    while (!is_ok) {
                        ValuesPanel settings (all_settings);
                        settings.activate();
                        char IP_buffer[INET_ADDRSTRLEN];
                        if (inet_pton(AF_INET, server_addr.get_value_str().c_str(), IP_buffer) != 1) {
                            Info i("Invalid IP address");
                            i.activate();
                        } else if (!Protocol::is_unumber(server_port.get_value_str())) {
                            Info i("Invalid port");
                            i.activate();
                        } else {
                            is_ok = true;
                        }
                    }
                }
                break;
            case 2:
                {
                    client_exit(0);
                }
                break;
        }
    }

    } catch(...) {
        std::cerr << "Critical unknown error has occured" << std::endl;
        _exit(100);
    }
}
