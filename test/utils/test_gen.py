#! /usr/bin/env python3

from random import *
from time import *

def genMatrix(vert_count, a, b):
    return [[randint(a,b) for j in range(vert)] for i in range(vert)]
    
seed(time())

vert = int(input("Verticies ammount: "))
a,b = [int(i) for i in input("Range (a,b): ").split()]
out_file_name = input("Save to file (or none): ")
out_file = None
if (out_file_name.lower() != "none" and out_file_name != ""):
    out_file = open(out_file_name, "w")

test_matrix = genMatrix(vert, a, b) 
for row in test_matrix:
    for col in row:
        print(col, end=' ')
    print('\n')

if (out_file != None):
    for row in test_matrix:
        for col in row:
            out_file.write(str(col) + ' ')
        out_file.write('\n')
