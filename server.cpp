#define SERVER_VERSION "early3"
#define DEFAULT_PORT "25000"
#define MAX_CLIENT_QUEUE 5

#define OUT_FIELD_W 5 // Field width for elements of printed matrices

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <iostream>
#include <iomanip>
#include <deque>
#include <algorithm>
#include <cstring>

#include "matrix.hpp"
#include "graph.h"
#include "protocol.h"

using namespace Protocol;

namespace Service {

/* Service 1: Floyd_Wasrhall
 * input: DUnum (2x) : source & destination verticies
 *        DGraph : graph
 * output: DUnum : shortest path value
 *         DText (2x) : shortest path text description and additional information
 * returns: -1 : error during calculations
 *           0 : ok
 *           1 : connection was closed 
 */

int floyd_warshall(int socketfd) {
    DUnum path_src, path_dest;
    DGraph input_graph;
    SocketAcceptors::accept_data_unum(socketfd, path_src);
    SocketAcceptors::accept_data_unum(socketfd, path_dest);
    SocketAcceptors::accept_data_graph(socketfd, input_graph);

    graph_t vert_ammount = input_graph.graph.get_verticies();
    Matrix<Edge> weight_matrix = input_graph.graph.get_shortest_edge_matrix(); // in case of multiedges
    Matrix<int64_t> predecessor_matrix(vert_ammount, vert_ammount, 0);

    // Floyd-Warshall
    for (int k = 0; k < vert_ammount; k++) {
        for (int row = 0; row < vert_ammount; row++) {
            for (int col = 0; col < vert_ammount; col++) {
                if (weight_matrix[row][k] + weight_matrix[k][col] < weight_matrix[row][col]) { // SP changed
                    predecessor_matrix[row][col] = !predecessor_matrix[k][col] ? k : predecessor_matrix[k][col]; 
                }
                weight_matrix[row][col] = std::min(weight_matrix[row][col], weight_matrix[row][k] + weight_matrix[k][col]);
            }
        }
    }
   
    DUnum sp_val;
    DText sp_desc, add_info;
    std::stringstream sp_desc_str, add_info_str;    
    
    if (path_src.number < weight_matrix.row_size() && path_dest.number < weight_matrix.col_size()) {
        sp_val.number = weight_matrix[path_src.number][path_dest.number].weight;
    } else {
        sp_val.number = 0;
    }

    // Path reconstruction
    if (path_dest.number >= predecessor_matrix.row_size() && path_dest.number >= predecessor_matrix.col_size()) {
        sp_desc.text = "No path";
    } else {
        field_t prev_node = path_dest.number;
        std::deque<field_t> SP;
        SP.push_front(prev_node);
        while ((prev_node = predecessor_matrix[path_src.number][prev_node])) {
            SP.push_front(prev_node);
        }
        SP.push_front(path_src.number);
        std::deque<field_t>::const_iterator iter = SP.cbegin();
        while (iter + 1 != SP.cend()) {
            sp_desc_str << *iter << " -> ";
            iter++;
        }
        sp_desc_str << *iter;
        sp_desc.text = sp_desc_str.str();
    }

    add_info_str << "----- SP matrix -----" << std::endl;
    for (int row = 0; row < weight_matrix.row_size(); row++) {
        for (int col = 0; col < weight_matrix.col_size(); col++) {
            add_info_str << std::setw(OUT_FIELD_W) << weight_matrix[row][col].weight << ' ';
        }
        add_info_str << std::endl;
    }
    add_info_str << std::endl;

    add_info_str << "----- Predecessor matrix -----" << std::endl;
    for (int row = 0; row < predecessor_matrix.row_size(); row++) {
        for (int col = 0; col < predecessor_matrix.col_size(); col++) {
            add_info_str << std::setw(OUT_FIELD_W) << predecessor_matrix[row][col] << ' ';
        }
        add_info_str << std::endl;
    }
    add_info_str << std::endl;

    add_info.text = add_info_str.str();
    SocketSenders::send_data_unum(socketfd, sp_val);
    SocketSenders::send_data_text(socketfd, sp_desc);
    SocketSenders::send_data_text(socketfd, add_info);
    return 0;
}

}

void print_welcome() {
    std::cout << "--------------------" << std::endl;
    std::cout << "x-graph:server (ver." << SERVER_VERSION << ")" << std::endl;
    std::cout << "Victor K. MSU FCMC, 2019" << std::endl;
    std::cout << "--------------------" << std::endl << std::endl;
}

void error_halt(const std::string &desc, int err_code) {
    std::cout << "! ERROR: " << desc << std::endl;
    exit(err_code);
}

int main() { 

    /* Initialization */

    print_welcome();

    /* Collecting information about server */

    struct addrinfo *server;
    struct addrinfo server_hints;
    memset(static_cast<void *>(&server_hints), 0, sizeof(server_hints));
    server_hints.ai_family = AF_INET;                            // Request IPv4 address
    server_hints.ai_socktype = SOCK_STREAM;                      // Virtual channel is requested
    server_hints.ai_protocol = getprotobyname("tcp")->p_proto;   // TCP protocol is requested explicitly 
    server_hints.ai_flags = AI_PASSIVE;                          // Wildcard IP adress (for port binding) 
    
    if (getaddrinfo(NULL, DEFAULT_PORT, &server_hints, &server)) {
        error_halt("Unsuccessfull collection of information about localhost.", 1); 
    }
    std::cout << "Information about localhost was successfully collected." << std::endl;
    
    /* Server socker creation */

    int listener = socket(server->ai_family, server->ai_socktype, server->ai_protocol);
    if (listener == -1) {
        error_halt("Error occured during socket creation.", 2);
    }
    std::cout << "Listening socket was created." << std::endl;

    /* Binding and start of listening for clients connections */

    if(bind(listener, server->ai_addr, server->ai_addrlen) == -1) {
        error_halt("Unsuccessfull socket binding.", 3);
    }
    std::cout << "Socket binded to port: " << ntohs(reinterpret_cast<sockaddr_in*>(server->ai_addr)->sin_port) << std::endl;

    if(listen(listener, MAX_CLIENT_QUEUE) == -1) {
        error_halt("Error during listening start", 4);
    }
    std::cout << "Listening for new connections started..." << std::endl;
    
    /* Work loop - accepting connections sequentionally */

    while (true) {

        /* Creating new socket for server-client exchange */

        struct sockaddr_in client_addr;    // Client's adress - IPv4
        socklen_t client_addr_size = sizeof(client_addr);
        memset(&client_addr, sizeof(client_addr), 0);
        char client_ip_str[INET_ADDRSTRLEN];
        socklen_t client_ip_str_size = sizeof(client_ip_str);
        std::fill_n(client_ip_str, sizeof(client_ip_str), '\0');
        int new_client = accept(listener, reinterpret_cast<struct sockaddr*>(&client_addr), &client_addr_size);
        
        /* Actions after connection accceptance */

        inet_ntop(AF_INET, &client_addr.sin_addr, client_ip_str, client_ip_str_size);
        std::cout << "Connection accepted. Client's IP: " << client_ip_str << " Client's port: " << ntohs(client_addr.sin_port) << std::endl;

        Message received_mes;
        int acceptance_res;
       
        /* Message receiving and processing */

        while ((acceptance_res = SocketAcceptors::accept_message(new_client, received_mes)) == 0) {
            std::cout << "Message type: " << unhash_message_type(received_mes.get_type()) << " with parameter: " << received_mes.get_parameter() << std::endl;
            if (received_mes.get_type() == message_type::SERVICE) {
                if (received_mes.get_parameter() == 1) {
                    Service::floyd_warshall(new_client);
                }
            }
            else break;
        }
         
        if (acceptance_res == -1) {
            std::cout << "Illegal message received." << std::endl;
        } else if (acceptance_res == 1) {
            std::cout << "Connection was closed by client." << std::endl;
        } else {
            std::cout << "Protocol violation." << std::endl;
        }

        /* Full connection shutdown */

        if (close(new_client) == -1) {
            error_halt("Error during socket closing.", 5);
        }
        std::cout << "Connection was successfully closed on the server side." << std::endl;

    } 
    
    /* Free server resources */

    freeaddrinfo(server);
    return 0;
}
