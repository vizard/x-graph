#ifndef MATRIX_HPP_HEADER
#define MATRIX_HPP_HEADER

#include "matrix.h"

template <class T>
Matrix<T>::Matrix(unsigned long row, unsigned long col, const T val) : rows(row), columns(col) {   
    array.reserve(row);
    for (unsigned long i = 0; i < row; i++) {
        array.push_back(std::vector<T>(col, T())); 
    }
}

template <class T>
std::vector<T> &Matrix<T>::operator[](unsigned long index) {
    return array[index];
}

template <class T>
const std::vector<T> &Matrix<T>::operator[](unsigned long index) const {
    return array[index];
}

template <class T>
unsigned long Matrix<T>::row_size() const {
    return rows; 
}

template <class T>
unsigned long Matrix<T>::col_size() const {
    return columns; 
}

#endif
