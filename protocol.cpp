#include "protocol.h"

namespace Protocol {

/* SocketAcceptors */

/* All acceptors return: 
 * 0 if OK
 * 1 if connection was closed
 * -1 if illegal packet was received
 */

int SocketAcceptors::accept_message(int socketfd, Message &mes) {
    char buffer[Buffer::msg_len + 1] = {'\0'};
    if (recv_all(socketfd, Buffer::msg_len, buffer, Buffer::msg_len) != Buffer::msg_len) {
        return 1;
    } 
    std::string buffer_str(buffer);
    mes = Message::parse_message(buffer_str);
    if (mes.get_type() == message_type::ERROR) {
        return -1;  // Error details are in mes' parameter
    }
    return 0;
}

int SocketAcceptors::accept_data_graph(int socketfd, DGraph &dgr) {
    Message mes;
    int header_get_res = accept_message(socketfd, mes);
    if (header_get_res) return header_get_res;
    if (mes.get_type() != message_type::DATA_GRAPH) return -1;
    field_t edg = mes.get_parameter();  
    for (field_t edg_counter = 0; edg_counter < edg; edg_counter++) {
        char edge_buffer[Buffer::edge_len + 1] = {'\0'};
        if (recv_all(socketfd, Buffer::edge_len, edge_buffer, Buffer::edge_len) != Buffer::edge_len) {
            return 1;
        }
        std::string edge_buffer_str(edge_buffer); 
        TEdge new_edge = TEdge::parse_edge(edge_buffer_str);
        if (new_edge.get_type() != dedge_type::STD) {
            return -1;
        }
        dgr.graph.add_edge(new_edge);
    }
    return 0;
}

int SocketAcceptors::accept_data_text(int socketfd, DText &dtxt) {
    constexpr char EOS = '#';
    Message mes;
    int header_get_res = accept_message(socketfd, mes);
    if (header_get_res) return header_get_res;
    if (mes.get_type() != message_type::DATA_TEXT) return -1;
    char buffer[Buffer::text_len + 1] = {'\0'};
    int receive_res;
    while ((receive_res = recv_all(socketfd, Buffer::text_len, buffer,Buffer::text_len) == Buffer::text_len)) {
        if (buffer[Buffer::text_len-1] == EOS) {
            int c = Buffer::text_len;
            while (buffer[--c] == EOS); 
            buffer[++c] = '\0';
            dtxt.text.append(buffer);
            return 0;
        } else {
            dtxt.text.append(buffer);
        }
    }
    return 1;
}

int SocketAcceptors::accept_data_unum(int socketfd, DUnum &num) {
    Message mes;
    int header_get_res = accept_message(socketfd, mes);
    if (header_get_res) return header_get_res;
    if (mes.get_type() != message_type::DATA_UNUM) return -1;
    num.number = mes.get_parameter(); 
    return 0;
}


/* SocketSenders */

int SocketSenders::send_message(int socketfd, Message &mes) {
    if (mes.get_type() == message_type::UNKNOWN) {
        return -1;
    }
    std::string mes_str = "<x-graph:";
    std::string type_str = unhash_message_type(mes.get_type());
    type_str.append(std::max<int>(10 - type_str.length(), 0), ' ');
    mes_str += type_str + ":";
    std::string param_str = std::to_string(mes.get_parameter());
    param_str.append(std::max<int>(10 - param_str.length(),0), ' ');
    mes_str += param_str + '>';
    if (send_all(socketfd, Buffer::msg_len, mes_str.c_str(), Buffer::msg_len) != Buffer::msg_len) {
        return 1;
    }
    return 0;
}

int SocketSenders::send_data_graph(int socketfd, DGraph &dgr) {
    Message msg(message_type::DATA_GRAPH, dgr.graph.size());
    int msg_res = SocketSenders::send_message(socketfd, msg);
    if (msg_res != 0) return msg_res;
    for (auto ptr = dgr.graph.cbegin(); ptr != dgr.graph.cend(); ptr++) {
        if (send_all(socketfd, Buffer::edge_len, TEdge(ptr->second).to_string().c_str(), Buffer::edge_len) != Buffer::edge_len) {
            return 1;
        } 
    }
    return 0;
}

int SocketSenders::send_data_text(int socketfd, DText &dtxt) {
    constexpr char EOS = '#';
    Message msg(message_type::DATA_TEXT, 0);
    int msg_res = SocketSenders::send_message(socketfd, msg);
    if (msg_res != 0) return msg_res;
    const size_t pack_num = dtxt.text.length() / static_cast<size_t>(Buffer::text_len);
    for (size_t pack_count = 0; pack_count < pack_num; pack_count++) {
        if (send_all(socketfd, Buffer::text_len, dtxt.text.substr(pack_count * 100u, 100).c_str(), Buffer::text_len) 
                != Buffer::text_len) {
            return 1;
        }
    } 
    std::string rest = dtxt.text.substr(pack_num * 100u, dtxt.text.length() % static_cast<size_t>(Buffer::text_len));
    rest.append(100 - rest.length(), EOS);
    if (send_all(socketfd, Buffer::text_len, rest.c_str(), Buffer::text_len) != Buffer::text_len) {
        return 1;
    }
    return 0;
}

int SocketSenders::send_data_unum(int socketfd, DUnum &num) {
    Message msg(message_type::DATA_UNUM, num.number);
    int msg_res = SocketSenders::send_message(socketfd, msg);
    return msg_res;
}


/* Message */

Message::Message() : type(message_type::UNKNOWN), param(0) {}

Message::Message(message_type t, field_t p) : type(t), param(p) {}

message_type Message::get_type() {
    return type;
} 

field_t Message::get_parameter() {
    return param;
}

Message Message::parse_message (const std::string &mes_str) {
    std::string protocol, mes_type_str, mes_param;
    message_type mes_type;
    if (mes_str.length() != Buffer::msg_len || *mes_str.begin() != '<' || *mes_str.rbegin() != '>' || 
            mes_str[8] != ':' || mes_str[19] != ':') {
        return Message(message_type::ERROR, 1); 
    }
    protocol = mes_str.substr(1,7);
    mes_type_str = mes_str.substr(9,10);
    mes_param = mes_str.substr(20,10);
    protocol.erase(remove_if(protocol.begin(), protocol.end(), isspace), protocol.end());
    mes_type_str.erase(remove_if(mes_type_str.begin(), mes_type_str.end(), isspace), mes_type_str.end());
    mes_param.erase(remove_if(mes_param.begin(), mes_param.end(), isspace), mes_param.end());
    if (protocol != "x-graph") {
        return Message(message_type::ERROR, 2);
    }
    switch (static_cast<mess_t>(hash_message_type(mes_type_str))) {
        case static_cast<mess_t>(message_type::ERROR):
            mes_type = message_type::ERROR; 
            break;
        case static_cast<mess_t>(message_type::SERVICE):
            mes_type = message_type::SERVICE; 
            break;
        case static_cast<mess_t>(message_type::DATA_GRAPH):
            mes_type = message_type::DATA_GRAPH; 
            break;
        case static_cast<mess_t>(message_type::DATA_TEXT):
            mes_type = message_type::DATA_TEXT; 
            break;
        case static_cast<mess_t>(message_type::DATA_UNUM):
            mes_type = message_type::DATA_UNUM; 
            break;
        case static_cast<mess_t>(message_type::CR):
            mes_type = message_type::CR; 
            break;
        case static_cast<mess_t>(message_type::SR):
            mes_type = message_type::SR; 
            break;
        case static_cast<mess_t>(message_type::UNKNOWN):
            return Message(message_type::ERROR, 3);
            break;
    }
    if (!is_unumber(mes_param)) {
        return Message(message_type::ERROR, 4);
    }
    return Message(mes_type, std::stoul(mes_param)); // Implementation defined
}


/* TEdge */

TEdge::TEdge(edge_ind_t src, edge_ind_t dest, weight_t weight, dedge_type t) : Edge(src,dest,weight), type(t) {}

TEdge::TEdge(Edge e) : Edge(e) {}

dedge_type TEdge::get_type() const {
    return type;
}

TEdge TEdge::parse_edge(std::string edg_str) {
    std::string source, destination, weight;
    if (edg_str.length() != Buffer::edge_len || *edg_str.begin() != '<' || *edg_str.rbegin() != '>' || 
            edg_str[11] != ':' || edg_str[22] != ':') {
        if (is_end_marker(edg_str)) {
            return TEdge(0,0,0,dedge_type::END);
        }
        return TEdge(0,0,0,dedge_type::ERR); 
    }
    source = edg_str.substr(1, 10);
    destination = edg_str.substr(12,10);
    weight = edg_str.substr(23,10);
    source.erase(remove_if(source.begin(), source.end(), isspace), source.end());
    destination.erase(remove_if(destination.begin(), destination.end(), isspace), destination.end());
    weight.erase(remove_if(weight.begin(), weight.end(), isspace), weight.end());
    if (!is_unumber(source) || !is_unumber(destination) || !is_unumber(weight)) {
        return TEdge(0,0,0,dedge_type::ERR); 
    }
    return TEdge(std::stoul(source), std::stoul(destination), std::stoul(weight), dedge_type::STD); // Implementation defined
}

std::string TEdge::to_string() const {
    std::string source_str, dest_str, weight_str, res;       // Implementation defined
    res.reserve(Buffer::edge_len);
    source_str = std::to_string(source).substr(0,10);
    dest_str = std::to_string(destination).substr(0,10);
    weight_str = std::to_string(weight).substr(0,10);
    source_str.append(10 - source_str.length(), ' ');
    dest_str.append(10 - dest_str.length(), ' ');
    weight_str.append(10 - weight_str.length(), ' ');
    res = "<" + source_str + ":" + dest_str + ":" + weight_str + ">";
    return res;
}

bool TEdge::is_end_marker(std::string &str) {
    if (str.length() != Buffer::edge_len || *str.begin() != '<' || *str.rbegin() != '>') {
        return false;
    }
    for (auto itr = str.begin() + 1; itr != str.end() - 1; itr++)
        if (*itr != '*') {
            return false;
        }
    return true;
}


/* Socket functions */

int recv_all(int socketfd, int message_len, char *buf, size_t buf_size) { // REWRITE!!
    if (static_cast<size_t>(message_len) > buf_size) {
        return -1;
    } 
    int read_bytes = 0;
    while (read_bytes < message_len) {
        int received = recv(socketfd, buf + read_bytes, buf_size - read_bytes, 0);
        if (!received) {
            return read_bytes;
        }
        read_bytes += received;
    }
    return read_bytes;
}

int send_all(int socketfd, int message_len, const char *buf, size_t buf_size) {
    if (static_cast<size_t>(message_len) > buf_size) {
        return -1;
    }
    int sent_bytes = 0;
    while (sent_bytes < message_len) {
        int sent = send(socketfd, buf + sent_bytes, buf_size - sent_bytes, 0);
        if (!sent) {
            return sent_bytes;
        }
        sent_bytes += sent;
    }
    return sent_bytes;
}



/* Other functions */

message_type hash_message_type(std::string str) {
    if (str == "ERROR") return message_type::ERROR;
    else if (str == "SERVICE") return message_type::SERVICE;
    else if (str == "DATA_GRAPH") return message_type::DATA_GRAPH;
    else if (str == "DATA_TEXT") return message_type::DATA_TEXT;
    else if (str == "DATA_UNUM") return message_type::DATA_UNUM;
    else if (str == "CR") return message_type::CR;
    else if (str == "SR") return message_type::SR;
    else return message_type::UNKNOWN;
}

std::string unhash_message_type(message_type t) {
    if (t == message_type::ERROR || t == message_type::UNKNOWN) return "ERROR";
    else if (t == message_type::SERVICE) return "SERVICE";
    else if (t == message_type::DATA_GRAPH) return "DATA_GRAPH";
    else if (t == message_type::DATA_TEXT) return "DATA_TEXT";
    else if (t == message_type::DATA_UNUM) return "DATA_UNUM";
    else if (t == message_type::CR) return "CR";
    if (t == message_type::SR) return "SR";
    else return "MESTYPEERR";
}

bool is_unumber(const std::string &str) {
    std::string::const_iterator iter = str.begin();
    while (iter != str.end() && std::isdigit(*iter)) iter++;
    return iter == str.end() ? true : false;
}

}
