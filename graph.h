#ifndef GRAPH_HEADER
#define GRAPH_HEADER

#include <map>
#include <utility>
#include <cstdint>
#include "matrix.h"

using weight_t = int64_t;
using edge_ind_t = int64_t;
using graph_t = unsigned long;

class Edge {
public:
    static constexpr weight_t inf = -1;
    weight_t weight;
    edge_ind_t source, destination;
    Edge();
    Edge(edge_ind_t src, edge_ind_t dest, weight_t weight = 0);
    friend bool operator> (const Edge &e1, const Edge &e2);
    friend bool operator< (const Edge &e1, const Edge &e2);
    friend bool operator== (const Edge &e1, const Edge &e2);
    friend Edge operator+ (const Edge &e1, const Edge &e2);
};

class EdgePredicate {
    public:
    bool operator()(const std::pair<graph_t, graph_t> &a, const std::pair<graph_t, graph_t> &b) {
        if (a.first < b.first) {
            return true;
        } else if (a.first == b.first && a.second< b.second) {
            return true;
        } else {
            return false;
        }
    }
};

class Graph {
public:
using edge_multimap = std::multimap<std::pair<graph_t, graph_t>, Edge, EdgePredicate>;
private:
graph_t verticies = 0;
edge_multimap graph_container;
public:
    graph_t get_verticies();
    Matrix<weight_t> get_weight_matrix();
    Matrix<Edge> get_shortest_edge_matrix();
    void add_edge(const Edge &e); 
    edge_multimap::const_iterator cbegin() const; 
    edge_multimap::const_iterator cend() const; 
    edge_multimap::iterator begin();
    edge_multimap::iterator end();
    edge_multimap::size_type size() const;
};

#endif
